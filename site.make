core = 7.x
api = 2

; Entity Connect
projects[entityconnect][type] = "module"
projects[entityconnect][download][type] = "git"
projects[entityconnect][download][url] = "https://git.uwaterloo.ca/drupal-org/entityconnect.git"
projects[entityconnect][download][tag] = "7.x-1.0-rc1"

; Quicktabs
projects[quicktabs][type] = "module"
projects[quicktabs][download][type] = "git"
projects[quicktabs][download][url] = "https://git.uwaterloo.ca/drupal-org/quicktabs.git"
projects[quicktabs][download][tag] = "7.x-3.6"
projects[quicktabs][subdir] = "contrib"

; Homepage Alerts (Major) content type
projects[uw_ct_homepage_alerts_major][type] = "module"
projects[uw_ct_homepage_alerts_major][download][type] = "git"
projects[uw_ct_homepage_alerts_major][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_alerts_major.git"
projects[uw_ct_homepage_alerts_major][download][tag] = "7.x-1.2"

; Homepage Audience Panel content type
projects[uw_ct_homepage_audience_panel][type] = "module"
projects[uw_ct_homepage_audience_panel][download][type] = "git"
projects[uw_ct_homepage_audience_panel][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_audience_panel.git"
projects[uw_ct_homepage_audience_panel][download][tag] = "7.x-1.1"

; Homepage Blocks content type
projects[uw_ct_homepage_blocks][type] = "module"
projects[uw_ct_homepage_blocks][download][type] = "git"
projects[uw_ct_homepage_blocks][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_blocks.git"
projects[uw_ct_homepage_blocks][download][tag] = "7.x-1.2"

; Homepage Events content type
projects[uw_ct_homepage_events][type] = "module"
projects[uw_ct_homepage_events][download][type] = "git"
projects[uw_ct_homepage_events][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_events.git"
projects[uw_ct_homepage_events][download][tag] = "7.x-1.3"

; Homepage Feature Stories content type
projects[uw_ct_homepage_feature_stories][type] = "module"
projects[uw_ct_homepage_feature_stories][download][type] = "git"
projects[uw_ct_homepage_feature_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_feature_stories.git"
projects[uw_ct_homepage_feature_stories][download][tag] = "7.x-1.3"

; Homepage News content type
projects[uw_ct_homepage_news][type] = "module"
projects[uw_ct_homepage_news][download][type] = "git"
projects[uw_ct_homepage_news][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_news.git"
projects[uw_ct_homepage_news][download][tag] = "7.x-1.5"

; Homepage one button
projects[uw_homepage_one_button][type] = "module"
projects[uw_homepage_one_button][download][type] = "git"
projects[uw_homepage_one_button][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_one_button.git"
projects[uw_homepage_one_button][download][tag] = "7.x-1.0"

; Homepage Positioning Stories content type
projects[uw_ct_homepage_positioning_stories][type] = "module"
projects[uw_ct_homepage_positioning_stories][download][type] = "git"
projects[uw_ct_homepage_positioning_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_positioning_stories.git"
projects[uw_ct_homepage_positioning_stories][download][tag] = "7.x-1.3"

; Homepage Positioning Text content type
projects[uw_ct_homepage_positioning_text][type] = "module"
projects[uw_ct_homepage_positioning_text][download][type] = "git"
projects[uw_ct_homepage_positioning_text][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_positioning_text.git"
projects[uw_ct_homepage_positioning_text][download][tag] = "7.x-1.1"

; Homepage Top Links content type
projects[uw_ct_homepage_top_links][type] = "module"
projects[uw_ct_homepage_top_links][download][type] = "git"
projects[uw_ct_homepage_top_links][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_homepage_top_links.git"
projects[uw_ct_homepage_top_links][download][tag] = "7.x-1.1"

; Daily Bulletin block
projects[uw_daily_bulletin_block][type] = "module"
projects[uw_daily_bulletin_block][download][type] = "git"
projects[uw_daily_bulletin_block][download][url] = "https://git.uwaterloo.ca/wcms/uw_daily_bulletin_block.git"
projects[uw_daily_bulletin_block][download][tag] = "7.x-1.6"

; Homepage CKEditor Additionals
projects[uw_homepage_ckeditor_additionals][type] = "module"
projects[uw_homepage_ckeditor_additionals][download][type] = "git"
projects[uw_homepage_ckeditor_additionals][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_ckeditor_additionals.git"
projects[uw_homepage_ckeditor_additionals][download][tag] = "7.x-1.5"

; Homepage Content Quickadd
projects[uw_homepage_content_quickadd][type] = "module"
projects[uw_homepage_content_quickadd][download][type] = "git"
projects[uw_homepage_content_quickadd][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_content_quickadd.git"
projects[uw_homepage_content_quickadd][download][tag] = "7.x-1.2"

; Homepage Content Type Addons
projects[uw_homepage_content_type_addons][type] = "module"
projects[uw_homepage_content_type_addons][download][type] = "git"
projects[uw_homepage_content_type_addons][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_content_type_addons.git"
projects[uw_homepage_content_type_addons][download][tag] = "7.x-1.8"

; Homepage Dashboard Fix
projects[uw_homepage_dashboard_fix][type] = "module"
projects[uw_homepage_dashboard_fix][download][type] = "git"
projects[uw_homepage_dashboard_fix][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_dashboard_fix.git"
projects[uw_homepage_dashboard_fix][download][tag] = "7.x-1.5"

; Homepage Directories
projects[uw_homepage_directories][type] = "module"
projects[uw_homepage_directories][download][type] = "git"
projects[uw_homepage_directories][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_directories.git"
projects[uw_homepage_directories][download][tag] = "7.x-1.9"

; Homepage Feature Stories
projects[uw_homepage_feature_stories][type] = "module"
projects[uw_homepage_feature_stories][download][type] = "git"
projects[uw_homepage_feature_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_feature_stories.git"
projects[uw_homepage_feature_stories][download][tag] = "7.x-1.2"

; Homepage Image Styles
projects[uw_homepage_image_styles][type] = "module"
projects[uw_homepage_image_styles][download][type] = "git"
projects[uw_homepage_image_styles][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_image_styles.git"
projects[uw_homepage_image_styles][download][tag] = "7.x-1.3"

; Homepage Permission Overrides
projects[uw_homepage_permission_overrides][type] = "module"
projects[uw_homepage_permission_overrides][download][type] = "git"
projects[uw_homepage_permission_overrides][download][url] = "https://git.uwaterloo.ca/wcms/uw_homepage_permission_overrides.git"
projects[uw_homepage_permission_overrides][download][tag] = "7.x-1.2"

; Main Site Controller
projects[uw_site_main][type] = "module"
projects[uw_site_main][download][type] = "git"
projects[uw_site_main][download][url] = "https://git.uwaterloo.ca/wcms/uw_site_main.git"
projects[uw_site_main][download][tag] = "7.x-2.11"

; Weather Station block
projects[uw_weather_station_block][type] = "module"
projects[uw_weather_station_block][download][type] = "git"
projects[uw_weather_station_block][download][url] = "https://git.uwaterloo.ca/wcms/uw_weather_station_block.git"
projects[uw_weather_station_block][download][tag] = "7.x-1.2"

; Workbench files
projects[workbench_files][type] = "module"
projects[workbench_files][download][type] = "git"
projects[workbench_files][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench_files.git"
projects[workbench_files][download][tag] = "7.x-1.0"

; Homepage Theme
projects[uw_home_theme][type] = "theme"
projects[uw_home_theme][download][type] = "git"
projects[uw_home_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_home_theme.git"
projects[uw_home_theme][download][tag] = "7.x-4.22"

; UW web tokens
projects[uw_web_tokens][type] = "module"
projects[uw_web_tokens][download][type] = "git"
projects[uw_web_tokens][download][url] = "https://git.uwaterloo.ca/wcms/uw_web_tokens.git"
projects[uw_web_tokens][download][tag] = "7.x-1.0"

