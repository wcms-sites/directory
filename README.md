## ASSUMPTIONS:

```
You have existing settings.php with sites Database and User info. 
You have latest production db dump "Cloned" to sites Database
You have working {drupal_profile} which includes core and uw_base_profile.
You have working {sitename} via Apache (redirect, aliases, rewrites), etc.
You have proper access rights to directories, git.uwaterloo.ca
If needed: You have homepage-files.zip  (archive of production files)
```
## INSTRUCTIONS:

```
cd {drupal_profile}/sites/{sitename}/
git clone https://git.uwaterloo.ca/wcms-sites/uwaterloo.ca.git makefile
git checkout {latest tag}
cd makefile
drush make --working-copy --force-gitinfofile --no-core -y --contrib-destination=. makefile/site.make
cd ..
drush cc
drush updb -y
drush fra -y
drush cc
```

## IF NEEDED:

>unzip homepage-files.zip files to files folder on local site to get images
>drush dis varnish -y (if you are running on a server without Varnish)



## *Troubleshooting*

```
Make sure you have a .htaccess file inside your site's file folder.  If you do not, then I recommend copying it from another site's file folder.
Make sure you have a .htaccess file inside your site's file/private folder.  If you do not, then I recommend copying it from another site's file/private folder.
```



New way to install local dev site to use the uwaterloo.ca homepage database.
For example, fresh install fdsu5 which will use the pilots.uwaterloo.ca homepage database.

Step 1: Drop fdsu5 database. drush sql-drop

Step 2: Prepare home.sql from https://wms-devops.private.uwaterloo.ca/PMA/index.php by selecting pilot.uwaterloo site and export to home.sql.

Step 3: Upload home.sql to fdsu5 local folder, then drush sql-cli < home.sql

Step 4: Get makefile. git clone https://git.uwaterloo.ca/wcms-sites/uwaterloo.ca.git makefile, and cd makefile, then 'drushmake'

Step 5: Get stage_file_proxy module.  git clone https://git.uwaterloo.ca/drupal-org/stage_file_proxy.git, then drush en stage_file_proxy

Step 6: drush cc all, then go to admin/config/system/stage_file_proxy, Set:
The origin website: https://pilots.uwaterloo.ca
The origin diectory: sites/uwaterloo.ca/files
Uncheck Imagecache Root
Check Hotlink
click Save configuration button.

Step 7: drush updb, drush fra -y, drush cc all, drush cc all, then check https://d7/fdsu5. It will display all conetents with images from pilots.uwaterloo.ca
